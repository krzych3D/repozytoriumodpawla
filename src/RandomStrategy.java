import java.util.Random;

/**
 * Created by amen on 8/21/17.
 */
public class RandomStrategy implements IInputStrategy {
    Random generator = new Random();

    public RandomStrategy() {
    }

    @Override
    public String getString() {
        return  "abcd";
    }

    @Override
    public double getDouble() {
        return generator.nextDouble();
    }

    @Override
    public int getInt() {
        return  generator.nextInt();
    }
}
